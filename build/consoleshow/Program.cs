﻿using System;
using System.Threading;
using System.IO;
using System.Drawing;

namespace consoleshow
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random randomNum = new Random();

            for (; ; )
            {
                for (int i = 0; i <= 1000; i++)
                {

                    int randomForegroundColor = randomNum.Next(1, 5);
                    switch (randomForegroundColor)
                    {
                        case 1:
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        case 2:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            break;
                        case 3:
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                        case 4:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;
                        case 5:
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            break;
                    }
                    
                    int randomBackgroundColor = randomNum.Next(1, 5);
                    switch (randomBackgroundColor)
                    {
                        case 1:
                            Console.BackgroundColor = ConsoleColor.Red;
                            break;
                        case 2:
                            Console.BackgroundColor = ConsoleColor.Blue;
                            break;
                        case 3:
                            Console.BackgroundColor = ConsoleColor.Green;
                            break;
                        case 4:
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            break;
                        case 5:
                            Console.BackgroundColor = ConsoleColor.Magenta;
                            break;
                    }
                    
                    int num = randomNum.Next(1, 9);
                    string nums = "";
                    for (int j = 0;j<=200;j++)
                    {
                        nums = nums+(randomNum.Next(1, 9)).ToString();
                    }

                    Console.Title = nums;
                    Console.Write(num);
                }
            }
        }
    }
}